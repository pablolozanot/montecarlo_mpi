#include <iostream> // std::cout
#include <vector> // std::vector
#include <mpi.h>
#define SIMPLE_SPRNG
#define USE_MPI
#include "sprng.h"
#include <cmath>
#include <sstream>
#include <cstdlib>     /* srand, rand */
#include <armadillo>
#include <algorithm> // std::generate

#include "MPI_Utilities/MPI_Utilities.h"
const double  PI =  4*std::atan(1.0);

double my_normal_sprng()
{
	double r1 =sprng();
	double r2 = sprng();
	return std::sqrt(-2*std::log(r1))*std::cos(2*PI*r2);
}

int main(int argc, char* argv[])
{
	MPI_Init(NULL,NULL);
	std::istringstream ss(argv[2]);
	
	int gtype=1;

	int n_start=0;
	int n_stop;
	int total_samples;
	if(argc >=2)
	{
		if (!(ss >> total_samples))
		{
			std::cerr << "Invalid number " << argv[2] << '\n';
			return 1;
		}
	}
	else
	{
		std::cout << "Not enough input arguments" << std::endl;
		return 1;
	}
	n_stop=total_samples;


	int rank, size, tag,seed;
	MPI_Comm comm;
	MPI_Status status;

	int n_simulations;//Number 
	comm  = MPI_COMM_WORLD;

	tag = 1;

	MPI_Comm_rank(comm, &rank);            
	MPI_Comm_size(comm, &size);
	int comm_size = n_stop/size;

	seed = make_sprng_seed();
	init_sprng(seed,SPRNG_DEFAULT,gtype);	/* initialize stream */

	MPI_Utilities::loop_controls(n_start,n_stop,size,rank);
	std::vector<double> rand_vec(n_stop-n_start);

	if(rank==0)
	{
		std::cout << "Running on " << size << " processors"<<std::endl;
	}

	std::generate(rand_vec.begin(),rand_vec.end(), my_normal_sprng);
	//Time to communicate

	if (rank == 0)
    {
      // The initial vector is the one in rank 0
		std::vector<double> recv_rand_vec(comm_size,0.0);

		// Add in contribution from other processes

		for (unsigned int source =1; source<size;source++)
	{
	  // receive recv_rand_vec from source and insert it in rand_vec 
		// all messages are tagged as zero 

	  tag = 0;

	  MPI_Recv(&recv_rand_vec[0],comm_size, MPI_DOUBLE, source, tag, comm, &status);
	  rand_vec.insert(rand_vec.end(), recv_rand_vec.begin(), recv_rand_vec.end());
	 
	}

		arma::mat rand_vec_arma(rand_vec); // Gaussian distribution
		rand_vec_arma.save("random_vector.txt",arma::raw_ascii);

    }
  else
    {
      // all other processes send their partial value to rank 0 

      tag = 0;
//Blocking synchronous send
      MPI_Ssend(&rand_vec[0],comm_size, MPI_DOUBLE, 0, tag, comm);
    }
	MPI_Finalize();
}



