#include "MPI_Utilities.h"
#include <iostream>
#include <ctime> /*time*/
#include <cmath>
//#define DEBUG
/*
int MPI_Utilities::seed_generator(const int& rank)
{
// http://arxiv.org/abs/1005.4117 //
	double t;
	double seed_rank=(double) rank;
	t = time (NULL);
	seed_rank = std::abs(std::fmod(((t*181)*((seed_rank-83)*359)),10472));
    int seed = (int) seed_rank;
	return seed;
};*/

void MPI_Utilities::loop_controls(int& n_start,int& n_stop,const int& size, const int& rank)
{
	if(rank==0)
	{
		n_start=0;
		n_stop=n_stop/size+(n_stop-(n_stop/size)*size);
		
	}
	else
	{
		n_start =(n_stop-(n_stop/size)*size) + rank * (n_stop / size);
		n_stop= n_start + n_stop/size;
	}

	#ifdef DEBUG
	std::cout << "Rank " << rank << " going from " << n_start << " to " << n_stop<<std::endl;
	#endif
	
};
