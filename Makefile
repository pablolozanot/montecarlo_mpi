FC = mpicxx
#FFLAGS= -O2
CURRENT_DIR = $(shell pwd)
FFLAGS= -O3 
SPRNG_DIR = /home/ezzpl/sprng5
FFHEADER = -I$(SPRNG_DIR)/include
#LIB = /usr/lib64/atlas/liblapack.so.3  /usr/lib64/atlas/libf77blas.so.3  /usr/lib64/atlas/libcblas.so.3  /usr/lib64/atlas/libatlas.so.3 -lgfortran # -lf77blas -lcblas -latlas -lgfortran
LIB = -L$(SPRNG_DIR)/lib -lsprng -lgmp -lgsl -lgslcblas -lm -larmadillo -llapack -lblas -lpthread
EXEC= main


SRC_PROG = main.cpp
SRC_PROG_1 =  MPI_Utilities/MPI_Utilities.cpp

OBJ_PROG = $(SRC_PROG:.cpp=.o)
OBJ_PROG_1 = $(SRC_PROG_1:.cpp=.o)





%.o: %.cpp
	$(FC) $(FFLAGS) $(FFHEADER) -c $< -o $*.o


all: $(EXEC)

$(EXEC) : $(OBJ_PROG) $(OBJ_PROG_1) $(OBJ_PROG_2) $(OBJ_PROG_3) $(OBJ_PROG_4) $(OBJ_PROG_5) $(OBJ_PROG_6) $(OBJ_PROG_7) $(OBJ_PROG_8)
		$(FC) $(FFLAGS) $(FFHEADER) -o $@ $^ $(LIB)

clean: 
	rm -rf $(OBJ_PROG)
	rm -rf $(OBJ_PROG_1)
	rm -rf $(OBJ_PROG_2)
	rm -rf $(OBJ_PROG_3)
	rm -rf $(OBJ_PROG_4)
	rm -rf $(OBJ_PROG_5)
	rm -rf $(OBJ_PROG_6)
	rm -rf $(OBJ_PROG_7)
	rm -rf $(OBJ_PROG_8)	
